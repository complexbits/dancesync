/*********************************************************************************
DanceSync is forked from ADXL345 code by Frankie Chu / Seeed studio, under the 
terms of the GNU GPL v2.1.  See README.txt for complete info.

Jamie Schwettmann
@complexbits

**********************************************************************************/

#include <Wire.h>
#include <ADXL345.h>


ADXL345 adxl; //variable adxl is an instance of the ADXL345 library

// Set the IRLED to pin 3 (PWM). (D3 on the Grove shield)
int IRLED = 3;

void setup(){
   
  // initialize the digital pin as an output.
  pinMode(IRLED, OUTPUT);  
  
  // Set the baud rate and power up the accelerometer
  Serial.begin(115200);
  adxl.powerOn();

  //all this stuff takes up memory. How much of it do we really need?

  //set activity/ inactivity thresholds (0-255)
  adxl.setActivityThreshold(75); //62.5mg per increment
  adxl.setInactivityThreshold(75); //62.5mg per increment
  adxl.setTimeInactivity(10); // how many seconds of no activity is inactive?
 
  //detect activity movement on this axes - 1 == on; 0 == off 
  adxl.setActivityX(1);
  adxl.setActivityY(1);
  adxl.setActivityZ(1);
 
  //detect inactivity movement on this axes - 1 == on; 0 == off
  adxl.setInactivityX(1);
  adxl.setInactivityY(1);
  adxl.setInactivityZ(1);
 
  //detect tap movement on this axes - 1 == on; 0 == off
  adxl.setTapDetectionOnX(1);
  adxl.setTapDetectionOnY(1);
  adxl.setTapDetectionOnZ(1);
 
  //set values for what is a tap, and what is a double tap (0-255)
  adxl.setTapThreshold(50); //62.5mg per increment
  adxl.setTapDuration(15); //625us per increment
  adxl.setDoubleTapLatency(80); //1.25ms per increment
  adxl.setDoubleTapWindow(200); //1.25ms per increment
 
  //set values for what is considered freefall (0-255)
  adxl.setFreeFallThreshold(7); //(5 - 9) recommended - 62.5mg per increment
  adxl.setFreeFallDuration(45); //(20 - 70) recommended - 5ms per increment
 
  //setting all interrupts to take place on int pin 1
  //I had issues with int pin 2, was unable to reset it
  adxl.setInterruptMapping( ADXL345_INT_SINGLE_TAP_BIT,   ADXL345_INT1_PIN );
  adxl.setInterruptMapping( ADXL345_INT_DOUBLE_TAP_BIT,   ADXL345_INT1_PIN );
  adxl.setInterruptMapping( ADXL345_INT_FREE_FALL_BIT,    ADXL345_INT1_PIN );
  adxl.setInterruptMapping( ADXL345_INT_ACTIVITY_BIT,     ADXL345_INT1_PIN );
  adxl.setInterruptMapping( ADXL345_INT_INACTIVITY_BIT,   ADXL345_INT1_PIN );
 
  //register interrupt actions - 1 == on; 0 == off  
  adxl.setInterrupt( ADXL345_INT_SINGLE_TAP_BIT, 1);
  adxl.setInterrupt( ADXL345_INT_DOUBLE_TAP_BIT, 1);
  adxl.setInterrupt( ADXL345_INT_FREE_FALL_BIT,  1);
  adxl.setInterrupt( ADXL345_INT_ACTIVITY_BIT,   1);
  adxl.setInterrupt( ADXL345_INT_INACTIVITY_BIT, 1);
}

void loop(){
  
 	int x,y,z;
        //read the accelerometer values and store them in variables  x,y,z
	adxl.readXYZ(&x, &y, &z); 
	/*****
        // Debug output on the serial console
        // Output x,y,z values 
	Serial.print("values of X , Y , Z: ");
	Serial.print(x);
	Serial.print(" , ");
	Serial.print(y);
	Serial.print(" , ");
	Serial.println(z);
	*************/

	double xyz[3];
	double ax,ay,az;
	adxl.getAcceleration(xyz);
	ax = xyz[0];
	ay = xyz[1];
	az = xyz[2];
        /*****
        // Debug output on the serial console
	Serial.print("X=");
	Serial.print(ax);
        Serial.println(" g");
	Serial.print("Y=");
	Serial.print(ay);
        Serial.println(" g");
	Serial.print("Z=");
	Serial.print(az);
        Serial.println(" g");
        *****/
        
        // Calculate accel vector length:
       
        double acveclen;
        double xyveclen;
        double x1 = double(x);
        double y1 = double(y);
        double z1 = double(z);
        xyveclen = hypot(x1,y1);
        acveclen = hypot(xyveclen,z1);
        
        // Debug
        // Serial.print("acveclen=");
        // Serial.println(acveclen);
         
        // Somehow, this line is read by processing in order to print a graph
        // on the screen? Mysterious.
        
 
	Serial.println("**********************");
	//delay(500);


/*

  //Fun Stuff!    
  //read interrupts source and look for triggered actions
 */ 
  //getInterruptSource clears all triggered actions after returning value
  //so do not call again until you need to recheck for triggered actions
   byte interrupts = adxl.getInterruptSource();
  /*
  // freefall
  if(adxl.triggered(interrupts, ADXL345_FREE_FALL)){
    Serial.println("freefall");
    //add code here to do when freefall is sensed
  } 
  
  //inactivity
  if(adxl.triggered(interrupts, ADXL345_INACTIVITY)){
    Serial.println("inactivity");
     //add code here to do when inactivity is sensed
  }
  */
  //activity
  //if(adxl.triggered(interrupts, ADXL345_ACTIVITY)){
    //Serial.println("activity"); 
     //add code here to do when activity is sensed
  
  //}
  /*
  //double tap
  if(adxl.triggered(interrupts, ADXL345_DOUBLE_TAP)){
    Serial.println("double tap");
     //add code here to do when a 2X tap is sensed
  }
  
  */
  
  // Lets try out tap triggering
  
  //tap
  if(adxl.triggered(interrupts, ADXL345_SINGLE_TAP)){
    
    // Debug output
    Serial.println("tap");
     //add code here to do when a tap is sensed
     Serial.println(Serial.print(acveclen));
     digitalWrite(IRLED, HIGH); 
     delay(50);
     digitalWrite(IRLED, LOW);
     
  }
 
}
