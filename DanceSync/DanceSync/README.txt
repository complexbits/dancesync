/*********************************************************************************
DanceSync is forked from ADXL345 code by Frankie Chu / Seeed studio. GNUGPL below.

Jamie Schwettmann
@complexbits

Done:

  - calculation of vector length for 3-axis accelerometer data (naturally rectified)
  - using tap detection on each axis to trigger IRLED HIGH.
  - Tested Galileo blink demo w/ red LED to verify I/O pinout to base shield.

To-do:

  - IR send/recv data
  - Update tap detection options to include vector length tap detection?
  - comparison of accelerometer peak frequency to IR recv frequency
  - display accelerometer frequency on IR send + test LED
  - display comparison/difference on visualization (software/LOL shield/simple LEDs?)
  -- computer visualization: 
  --- processing visualizations?
  --- X11 pixel manipulation?
  --- javascript visualization? 

*********************************************************************************/
/*****************************************************************************/
//	Function:    Get the accelemeter of X/Y/Z axis and print out on the 
//					serial monitor.
//  Hardware:    3-Axis Digital Accelerometer(16g)
//	Arduino IDE: Arduino-1.0
//	Author:	 Frankie.Chu		
//	Date: 	 Jan 11,2013
//	Version: v1.0
//	by www.seeedstudio.com
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public
//  License as published by the Free Software Foundation; either
//  version 2.1 of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//
/*******************************************************************************/
